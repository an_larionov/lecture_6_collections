package com.app.main;

import com.app.service.DemoService;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        DemoService.start();
    }
}
